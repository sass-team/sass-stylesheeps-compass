sass-stylesheets-compass (0.12.12-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * fix lintian overrides

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 14 Jun 2022 18:00:15 +0200

sass-stylesheets-compass (0.12.11-1) unstable; urgency=medium

  [ upstream ]
  * new release
    (no changes since last development snapshot)

  [ Jonas Smedegaard ]
  * update watch file: track git tags (not git commits)
  * update lintian overrides

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 18 Oct 2021 02:34:00 +0200

sass-stylesheets-compass (0.12.10+20210421-1) unstable; urgency=medium

  [ upstream ]
  * development snapshot;
    notable changes:
    + Adde support for column-span
      https://github.com/Compass/compass/blob/stable/core/stylesheets/compass/css3/_columns.scss
      https://developer.mozilla.org/en-US/docs/Web/CSS/column-span
    + import functions.scss in files that use compact() function
    + Remove -pie prefix from background()
    + add rhythm-margins in typography section
    + update _box_sizing.scss
    + update readme to reflect correct compass version support
    + add helper functions font-files() and font-url(),
      which are used in the @font-face mixin
    + use "@else if" instead of "@elseif"
      which is deprecated in future Sass versions

  [ Jonas Smedegaard ]
  * update watch file:
    + track git commits
      (which also ignores older but higher-ranking releases)
  * declare compliance with Debian Policy 4.6.0
  * set section to oldlibs,
    and mention upstream deprecation in short and long description
  * drop all patches,
    now all applied except one rejected merge request:
    see <https://github.com/Igosuki/compass-mixins/pull/102>
  * update copyright info:
    + use License-Grant and Reference;
      add lintian overrides about License-Reference field
    + update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 18 Sep 2021 16:07:39 +0200

sass-stylesheets-compass (0.12.10-3) unstable; urgency=medium

  * update install path to be /usr/share/sass
    (i.e. without additional stylesheets/ subdir),
    to align with other sass module packages;
    add NEWS file about the change

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 06 Mar 2021 15:09:29 +0100

sass-stylesheets-compass (0.12.10-2) unstable; urgency=medium

  * simplify source script copyright-check
  * use debhelper compatibility level 13 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * move maintenance to salsa debian group: update Vcs-* fields
  * set Rules-Requires-Root: no
  * declare compliance with Debian Policy 4.5.0
  * copyright:
    + update coverage
    + use https protocol in file format URL
    + drop alternate source URL
  * watch:
    + use substritution string @ANY_VERSION@
    + set dversionmangle=auto
      (even though currently unused
    + rewrite usage comment
  * add patches cherry-picked from upstream pending merge-requests:
    + fix add support for column-span()
    + import functions.scss where compact() is used
    + remove -pie prefix from background()
    + add support for rhythm-margins()
    + fix apply default for box-sizing()
    + fix use compass 0.12.7 parameter ordering for single-box-shadow()
    + use @else if instead of @elseif
  * override lintian for another long test line

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 31 Aug 2020 14:53:08 +0200

sass-stylesheets-compass (0.12.10-1) unstable; urgency=low

  * Initial release.
    Closes: bug#875581.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 12 Sep 2017 17:14:02 +0200
